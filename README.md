# LR3 (GOST 34.10-2015 Digital Signature)

Реализация стандарта электронной подписи ГОСТ Р 34.10─2012 и функции хеширования "Стрибог" ГОСТ Р 34.11─2012
на языке Java для третьей лабораторной работы по курсу "Криптографические протоколы" (осенний семестр 2018/2019 СПбГЭТУ).

студенты группы 4362

Георгица Андрей

Тищенко Виталия

* * *


Implementation of Russian digital signature standard GOST 34.10-2012 and "Streebog" hash-function from GOST 34.11-2012 in Java for the third lab work of сryptographic protocols course (fall semester of 2018/2019 academic year, Saint-Petersburg ETU)


Andrey Georgitsa

Tishchenko Vitaliya

* * *

Codeship: [ ![Codeship Status for andygeo/gost_34_10_digitalsign](https://app.codeship.com/projects/08394a00-e1ed-0136-a005-36b481714cf9/status?branch=master)](https://app.codeship.com/projects/318822)