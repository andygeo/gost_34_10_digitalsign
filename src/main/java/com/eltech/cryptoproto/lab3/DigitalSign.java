package main.java.com.eltech.cryptoproto.lab3;

import main.java.com.eltech.cryptoproto.lab3.Elliptic_curve.EllipticCurve;
import main.java.com.eltech.cryptoproto.lab3.Elliptic_curve.Point;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;



public class DigitalSign {
    /**
     * Shared digital sign scheme parameters
     */
    private EllipticCurve curve;
    private Point point;
    private BigInteger m;
    private BigInteger q;



    /**
     * Personal parameters
     */

    /**
     * Signing key
     */
    private BigInteger d;

    /**
     * Sign check key points
     */
    public Point Q;


    /**
     *
     * @param a - first coefficient of elliptic curve equation
     * @param b - second coefficient of elliptic curve equation
     * @param p - prime curve modulo
     * @param P - point, satisfied qP=0
     * @param m - curve group size (amount of points)
     * @param q - order of P cyclic subgroup
     * @param d - signing key
     * @throws IllegalArgumentException - if any rule from paragraph 5.2 is false
     */
    public DigitalSign(BigInteger a, BigInteger b, BigInteger p,Point P, BigInteger m, BigInteger q, BigInteger d) throws IllegalArgumentException{
        if (m.equals(p)) throw new IllegalArgumentException("Modulo p shouldn't be equal with m!");
        if (q.compareTo(d)<=0) throw new IllegalArgumentException("q<=d!");

        BigInteger invariant=BigInteger.valueOf(1728).multiply(
                divideFin(
                        a.pow(3).multiply(BigInteger.valueOf(4)),
                        a.pow(3).multiply(BigInteger.valueOf(4)).add(b.pow(2).multiply(BigInteger.valueOf(27))),
                        p))
                .remainder(p);
        if (invariant.equals(BigInteger.ZERO)||invariant.equals(BigInteger.valueOf(1728))) throw new IllegalArgumentException("Wrong curve invariant!");
        EllipticCurve c=new EllipticCurve(a,b,p);

        this.m=m;
        this.q=q;

        for (int i=0;i<132;i++){
            if (p.equals(BigInteger.ONE))throw new IllegalArgumentException("p pow t rule isn't true! ");
            p=p.multiply(p).remainder(q);
        }

        if(!c.onCurve(P)) throw new IllegalArgumentException("Point is not on the curve!");
        curve=c;
        point=P;
        this.d=d;
        this.Q=curve.multiply(d,point);
    }


    public DigitalSign(long a, BigInteger b, BigInteger p, Point P, BigInteger m, BigInteger q, BigInteger d){
        this (BigInteger.valueOf(a),b,p,P,m, q,d);
    }

    /**
     * Compute the exponent over the curve's modulo p
     * @param a - base
     * @param n - exponent
     * @return BigInteger number as a result
     */
    private BigInteger  modular_pow (BigInteger a, BigInteger n, BigInteger modulo){
        BigInteger result=new BigInteger("1");
        while (!n.equals(BigInteger.ZERO)){
            if (!n.and(BigInteger.ONE).equals(BigInteger.ZERO)){
                result=(result.multiply(a)).mod(modulo);
                n=n.subtract(BigInteger.ONE);
            } else {
                a=a.pow(2).mod(modulo);
                n=n.shiftRight(1);
            }
        }
        return  result;
    }

    /**
     * Division operation over finite field
     * @param a1 - dividend
     * @param a2 - divisor
     * @return BigInteger number as a result
     */
    private BigInteger divideFin(BigInteger a1, BigInteger a2, BigInteger modulo){
        BigInteger[] result= a1.divideAndRemainder(modulo);
        if (result[1].equals(BigInteger.ZERO)) return result[0].mod(modulo);
        BigInteger bMulInv=modular_pow(a2,modulo.subtract(BigInteger.valueOf(2)),modulo);
        return a1.multiply(bMulInv).mod(modulo);
    }

    /**
     * Example from Application A GOST 34.10-2012
     * @return array of bytes, containing digital signature
     */
    public byte[] signBytesTestExample(){
        BigInteger e=new BigInteger("20798893674476452017134061561508270130637142515379653289952617252661468872421");
        BigInteger k,r,s;
        k=new BigInteger("53854137677348463731403841147996619241504003434302020712960838528893196233395");
        r=curve.multiply(k,point).getX().remainder(q);
        s=r.multiply(d).add(k.multiply(e)).remainder(q);
        byte[] rByteArr=r.toByteArray();
        byte[] sByteArr=s.toByteArray();
        byte[] res=new byte[Math.max(rByteArr.length,sByteArr.length)*2];
        System.arraycopy(rByteArr,0,res,res.length/2-rByteArr.length,rByteArr.length);
        System.arraycopy(sByteArr,0,res,res.length-sByteArr.length,sByteArr.length);
        return res;
    }

    /**
     * Example from Application A GOST 34.10-2012
     * @param sign - digital signature
     * @return
     */
    public boolean checkSignTestExample(byte[] sign){
        BigInteger k,r,s;
        BigInteger e=new BigInteger("20798893674476452017134061561508270130637142515379653289952617252661468872421");
        r=new BigInteger("29700980915817952874371204983938256990422752107994319651632687982059210933395");
        k=new BigInteger("53854137677348463731403841147996619241504003434302020712960838528893196233395");
        s=r.multiply(d).add(k.multiply(e)).remainder(q);
        BigInteger z1=divideFin(s,e,q);
        BigInteger z2= divideFin(r.negate(),e,q);
        Point C=curve.add(curve.multiply(z1,point),curve.multiply(z2,Q));
        return Arrays.equals(C.getX().toByteArray(),Arrays.copyOfRange(sign,0,sign.length/2));
  }




    public byte[] calculateSignature(byte[] data, boolean mode){


        StreebogHasher hasher = new StreebogHasher();
        byte[] digest=hasher.calculateHash(data,mode);
        return signHash(digest);
    }

    public boolean verifySignature(byte[] data, byte[] signature, Point checkQ){
        StreebogHasher hasher = new StreebogHasher();
        boolean mode=true;
        if (signature.length==32) mode=false;
        if (signature.length!=64) ;
        byte[] digest=hasher.calculateHash(data,mode);
        return  checkHash(digest,signature,checkQ);
    }


    /**
     * Sign hash digest
     * @param block - hash digest to be signed
     * @return array of bytes, containing digital signature
     */
    public byte[] signHash(byte[] block){

        BigInteger e=new BigInteger(block).remainder(q);
        if (e.equals(BigInteger.ZERO)) e=BigInteger.ONE;
        BigInteger k,r,s;
        do {
            do {
                {
                    k=new BigInteger(q.bitLength(),new Random()).remainder(q);
                }
                r=curve.multiply(k,point).getX().remainder(q);
            } while (r.equals(BigInteger.ZERO));
            s=r.multiply(d).add(k.multiply(e)).remainder(q);
        }while (s.equals(BigInteger.ZERO));
        byte[] rByteArr=r.toByteArray();
        byte[] sByteArr=s.toByteArray();
        byte[] res=new byte[Math.max(rByteArr.length,sByteArr.length)*2];
        System.arraycopy(rByteArr,0,res,res.length/2-rByteArr.length,rByteArr.length);
        System.arraycopy(sByteArr,0,res,res.length-sByteArr.length,sByteArr.length);
        return res;
    }

    /**
     * Hash digest verification
     * @param block - hash digest to be verified
     * @param sign - digital sign digest array
     * @param checkQ - check sign point
     * @throws IllegalArgumentException - if s or r don't belong to the (0;q) interval or q don't correspond to hash digest size
     * @return true if signature is correct, false if it isn't
     */
    public boolean checkHash(byte[] block, byte[] sign, Point checkQ){
        BigInteger e=new BigInteger(block).remainder(q);
        if (e.equals(BigInteger.ZERO)) e=BigInteger.ONE;
        BigInteger r,s;
        r=new BigInteger(Arrays.copyOfRange(sign,0,sign.length/2));
        s=new BigInteger(Arrays.copyOfRange(sign,sign.length/2,sign.length));
        if (((r.compareTo(BigInteger.ZERO)<0)&&(r.compareTo(q)!=-1))||(((s.compareTo(BigInteger.ZERO)<0)&&(s.compareTo(q)!=-1)))) throw new IllegalArgumentException("The signature is wrong, s or r don't belong to (0;q)");
        BigInteger z1=divideFin(s,e,q);
        BigInteger z2= divideFin(r.negate(),e,q);
        Point C=curve.add(curve.multiply(z1,point),curve.multiply(z2,checkQ));
        return Arrays.equals(C.getX().toByteArray(),Arrays.copyOfRange(sign,0,sign.length/2));
    }
}
