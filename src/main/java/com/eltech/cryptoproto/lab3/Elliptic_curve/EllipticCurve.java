package main.java.com.eltech.cryptoproto.lab3.Elliptic_curve;
import java.math.BigInteger;

import main.java.com.eltech.cryptoproto.lab3.StreebogHasher;


public class EllipticCurve {
    private BigInteger a;
    private BigInteger b;
    private BigInteger p;
    public static Point infPoint = new Point(0,0);

    /**
     * Initialize curve
     * @param a - 1st curve's coefficient
     * @param b - 2nd curve's coefficient
     * @param p - prime module
     * @throws IllegalArgumentException if p isn't prime or curve discriminant is singular
     */
    public EllipticCurve(BigInteger a, BigInteger b, BigInteger p) throws IllegalArgumentException{
        if(!p.isProbablePrime(1000)) throw new IllegalArgumentException("P is not prime!");
        this.p=p;
        if (a.pow(3).multiply(BigInteger.valueOf(4)).add(b.pow(2).multiply(BigInteger.valueOf(27))).equals(BigInteger.ZERO))  throw new IllegalArgumentException ("Curve is singular!");
        this.a=a.mod(p);
        this.b=b.mod(p);
    }
    public EllipticCurve(long a, long b, BigInteger p) throws IllegalArgumentException{
        this (BigInteger.valueOf(a),BigInteger.valueOf(b),p);
    }
    public EllipticCurve(long a, long b, long p) throws IllegalArgumentException{
        this (BigInteger.valueOf(a),BigInteger.valueOf(b),BigInteger.valueOf(p));
    }
    public EllipticCurve(long a, BigInteger b, BigInteger p) throws IllegalArgumentException{
        this (BigInteger.valueOf(a),b,p);
    }

    /**
     * Curve example from GOST
     */
    public EllipticCurve(){
        this (BigInteger.valueOf(7),new BigInteger("43308876546767276905765904595650931995942111794451039583252968842033849580414"),new BigInteger("57896044618658097711785492504343953926634992332820282019728792003956564821041"));
    }

    /**
     * Check if the given point belongs to this curve
     * @param P - point to be checked
     * @return true if P belongs to the curve, false if not
     */
    public boolean onCurve(Point P){
        return P.y.pow(2).mod(this.p).equals(P.x.pow(3).add(this.a.multiply(P.x)).add(this.b).mod(this.p));
    }


    /**
     * Compute the exponent over the curve's modulo p
     * @param a - base
     * @param n - exponent
     * @return BigInteger number as a result
     */

    private BigInteger  modular_pow (BigInteger a, BigInteger n){
        BigInteger result=new BigInteger("1");
        while (!n.equals(BigInteger.ZERO)){
            if (!n.and(BigInteger.ONE).equals(BigInteger.ZERO)){
                result=(result.multiply(a)).mod(p);
                n=n.subtract(BigInteger.ONE);
            } else {
                a=a.pow(2).mod(p);
                n=n.shiftRight(1);
            }
        }
        return  result;
    }

    /**
     * Division operation over finite field
     * @param a1 - dividend
     * @param a2 - divisor
     * @return BigInteger number as a result
     */
    private BigInteger divideFin(BigInteger a1, BigInteger a2){
        BigInteger[] result= a1.divideAndRemainder(b);
        if (result[1].equals(BigInteger.ZERO)) return result[0].mod(p);
        BigInteger bMulInv=modular_pow(a2,p.subtract(BigInteger.valueOf(2)));
        return a1.multiply(bMulInv).mod(p);
    }


    /**
     * Calculate the sum of 2 points over the curve
     * @param P - first point
     * @param Q - second point
     * @return result point
     */
    public Point add (Point P, Point Q){
        if (P.equals(infPoint)) return Q;
        if (Q.equals(infPoint)) return P;
        if (P.equals(Q)) return doublePoint(P);
        if(P.x.equals(Q.x)) return infPoint;
        BigInteger s=divideFin(Q.y.subtract(P.y),Q.x.subtract(P.x));
        BigInteger rx=s.pow(2).subtract(P.x).subtract(Q.x).mod(p);
        BigInteger ry=s.multiply(P.x.subtract(rx)).subtract(P.y).mod(p);
        return new Point(rx,ry);
    }


    /**
     *  Double up the given point
     * (necessary for multiplication)
     * @param P - point to be doubled up
     * @return result point
     *
     */
    public Point doublePoint(Point P){
        BigInteger s=divideFin(P.x.pow(2).multiply(BigInteger.valueOf(3)).add(a),P.y.multiply(BigInteger.valueOf(2)));
        BigInteger rx=s.pow(2).subtract(P.x).subtract(P.x);
        BigInteger ry=s.multiply(P.x.subtract(rx)).subtract(P.y);
        return new Point(rx.mod(p),ry.mod(p));
    }


    /**
     * Multiply point by a number using
     * "Double-and-add" method
     * (binary representation of n)
     *
     * @param n - number
     * @param P - point
     * @return result point
     */
    public Point multiply (BigInteger n,Point P){
        if (n.equals(BigInteger.ONE)) return P;
        Point buf=P;
        String hex= n.toString(16);
        byte[] bytes= StreebogHasher.fromHexStrToByteArr(hex);
        Point res=new Point();
        for (int i=bytes.length-1;i>=0;i--){
            short mask=1;
            for (int j = 0; j <8 ; j++) {
                if ((bytes[i]&mask)!=0) {
                    res=add(res,buf);
                }
                buf=doublePoint(buf);
                mask<<=1;
            }
        }
        res.x=res.x.remainder(this.p);
        res.y=res.y.remainder(this.p);
        return res;
    }

    public Point multiply (long n,Point P){
        return multiply(BigInteger.valueOf(n),P);
    }




}
