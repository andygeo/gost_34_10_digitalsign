package main.java.com.eltech.cryptoproto.lab3.Elliptic_curve;
import java.math.BigInteger;

/**
 * Curve point implementation
 */
public class Point {
    public BigInteger x,y;

    public Point(long x, long y){
        this.x=BigInteger.valueOf(x);
        this.y=BigInteger.valueOf(y);
    }
    public Point(long x, BigInteger y){
        this.x=BigInteger.valueOf(x);
        this.y=y;
    }

    public Point(BigInteger x, BigInteger y){
        this.x=x;
        this.y=y;
    }
    public Point(){
        this.x=BigInteger.ZERO;
        this.y=BigInteger.ZERO;
    }
    public BigInteger getX(){
        return x;
    }
    public BigInteger getY(){
        return y;
    }

    @Override
    public String toString(){
        return new String("("+x.toString()+","+y.toString()+")");
    }

    public boolean isEmpty(){
        if (!x.equals(BigInteger.ZERO)) return false;
        if (!y.equals(BigInteger.ZERO)) return false;
        return true;
    }
    @Override
    public boolean equals (Object obj){
        if (obj == null)  return false;
        if (!Point.class.isAssignableFrom(obj.getClass()))  return false;
        final Point another =(Point) obj;
        if (!this.x.equals(another.x)) return false;
        if (!this.y.equals(another.y)) return false;
        return true;
    }

}
