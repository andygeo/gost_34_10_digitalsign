package test.java.com.eltech.cryptoproto.lab3;

import main.java.com.eltech.cryptoproto.lab3.Elliptic_curve.Point;
import main.java.com.eltech.cryptoproto.lab3.StreebogHasher;
import main.java.com.eltech.cryptoproto.lab3.DigitalSign;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import static main.java.com.eltech.cryptoproto.lab3.StreebogHasher.fromHexStrToByteArr;

public class DigSignTest {
    StreebogHasher hasher=new StreebogHasher();
    Point P=new Point(
            2,
            new BigInteger("4018974056539037503335449422937059775635739389905545080690979365213431566280")
    );

    DigitalSign signer1=new DigitalSign(
            7,
            new BigInteger("43308876546767276905765904595650931995942111794451039583252968842033849580414"),
            new BigInteger("57896044618658097711785492504343953926634992332820282019728792003956564821041"),
            P,
            new BigInteger("57896044618658097711785492504343953927082934583725450622380973592137631069619"),
            new BigInteger("57896044618658097711785492504343953927082934583725450622380973592137631069619"),
            new BigInteger("55441196065363246126355624130324183196576709222340016572108097750006097525544")
    );

    DigitalSign signer2=new DigitalSign(
            7,
            new BigInteger("43308876546767276905765904595650931995942111794451039583252968842033849580414"),
            new BigInteger("57896044618658097711785492504343953926634992332820282019728792003956564821041"),
            P,
            new BigInteger("57896044618658097711785492504343953927082934583725450622380973592137631069619"),
            new BigInteger("57896044618658097711785492504343953927082934583725450622380973592137631069619"),
            new BigInteger("45441196065363246126355624130324183196576709222340016572108097750006097525544")
    );

    @Test
    /**
     * Prove, that linearVectorColumns ar transposed linearVectors described in GOST 34.11-2012
     */
    public void test0_rowsTransposition(){
        StringBuffer strBuf=new StringBuffer();
        ArrayList <String> array=new ArrayList<>();
        for (String s:StreebogHasher.linVectors) {
            byte[] buf=fromHexStrToByteArr(s);
            strBuf.delete(0,strBuf.length());
            for (int i = 0; i <64 ; i++) {
                byte mask=(byte)((1<<(7-i%8))&0xFF);
                mask=(byte)((buf[i/8]&mask)&0xFF);
                if (mask!=0) strBuf.append(1);
                else strBuf.append(0);
            }
            array.add(strBuf.toString());
        }

        ArrayList <String> transposedArr=new ArrayList<>();
        for (int i = 0; i <64 ; i++) {
            strBuf.delete(0,strBuf.length());
            for (int j = 0; j < 64; j++) {
                strBuf.append(array.get(j).charAt(i));
            }
            transposedArr.add(strBuf.toString());
        }

        ArrayList<String> transposedArrHex=new ArrayList<>();
        for (String s:transposedArr) {

            strBuf.delete(0,strBuf.length());
            for (int i = 0; i < 16; i++) {
                String binary=s.substring(4*i,4*i+4);
                Integer integer=Integer.parseInt(binary,2);
                strBuf.append(Integer.toHexString(integer));
            }
            transposedArrHex.add(strBuf.toString());
        }

        for (int i = 0; i <transposedArrHex.size() ; i++) {
            Assert.assertEquals(StreebogHasher.linVectorsColumns[i], transposedArrHex.get(i));
        }
    }

    @Test
    public void test1_GOST_StreebogExamples(){
        String message1="323130393837363534333231303938373635343332313039383736353433323130393837363534333231303938373635343332313039383736353433323130";
        byte[] mess1Array= fromHexStrToByteArr(message1);
        Assert.assertEquals("486f64c1917879417fef082b3381a4e211c324f074654c38823a7b76f830ad00fa1fbae42b1285c0352f227524bc9ab16254288dd6863dccd5b9f54a1ad0541b",StreebogHasher.fromByteArrToHexString(hasher.calculateHash(mess1Array,true)));
        Assert.assertEquals("00557be5e584fd52a449b16b0251d05d27f94ab76cbaa6da890b59d8ef1e159d",StreebogHasher.fromByteArrToHexString(hasher.calculateHash(mess1Array,false)));

        String message2="fbe2e5f0eee3c820fbeafaebef20fffbf0e1e0f0f520e0ed20e8ece0ebe5f0f2f120fff0eeec20f120faf2fee5e2202ce8f6f3ede220e8e6eee1e8f0f2d1202ce8f0f2e5e220e5d1";
        byte[] mess2Array= fromHexStrToByteArr(message2);
        Assert.assertEquals("28fbc9bada033b1460642bdcddb90c3fb3e56c497ccd0f62b8a2ad4935e85f037613966de4ee00531ae60f3b5a47f8dae06915d5f2f194996fcabf2622e6881e",StreebogHasher.fromByteArrToHexString(hasher.calculateHash(mess2Array,true)));
        Assert.assertEquals("508f7e553c06501d749a66fc28c6cac0b005746d97537fa85d9e40904efed29d",StreebogHasher.fromByteArrToHexString(hasher.calculateHash(mess2Array,false)));
    }


    @Test
    public void test2_GOST_DS_signingExample(){
        byte[] signDigest=signer1.signBytesTestExample();
        Assert.assertEquals(
                "41aa28d2f1ab148280cd9ed56feda41974053554a42767b83ad043fd39dc0493",
                StreebogHasher.fromByteArrToHexString(Arrays.copyOfRange(signDigest,0,signDigest.length/2))
        );
        Assert.assertEquals(true,signer1.checkSignTestExample(signDigest));
    }

    @Test
    public void test3_correctSigningExample(){
        String s1="String1";
        byte[] block1=hasher.calculateHash(s1.getBytes(),StreebogHasher.mode512);
        byte[] digestSign1=signer1.signHash(block1);

        String s2="String1";
        byte[] block2=hasher.calculateHash(s2.getBytes(),StreebogHasher.mode512);
        Assert.assertEquals(true,signer2.checkHash(block2,digestSign1,signer1.Q));
    }

    @Test
    public void test4_wrongSigningExample(){
        String s1="String1";
        byte[] block1=hasher.calculateHash(s1.getBytes(),StreebogHasher.mode256);
        byte[] digestSign1=signer1.signHash(block1);

        String s2="String2";
        byte[] block2=hasher.calculateHash(s2.getBytes(),StreebogHasher.mode256);
        Assert.assertEquals(false,signer2.checkHash(block2,digestSign1,signer1.Q));
    }

    @Test
    public void test5_corruptedSignExample(){
        String s1="String1";
        byte[] block1=hasher.calculateHash(s1.getBytes(),StreebogHasher.mode512);
        byte[] digestSign1=signer1.signHash(block1);
        digestSign1[5]=12;//sign is corrupted
        String s2="String1";
        byte[] block2=hasher.calculateHash(s2.getBytes(),StreebogHasher.mode512);
        Assert.assertEquals(false,signer2.checkHash(block2,digestSign1,signer1.Q));
    }




}